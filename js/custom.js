(function ($) {
    "use strict";

    var dawatWindow = $(window);

    // Preloader Active Code
    dawatWindow.on("load", function () {
        $('#preloader').fadeOut('1000', function () {
            $(this).remove();
        });
    });

    // Countdown Active Code
    if ($.fn.countdown) {
        $('#eventCoundown').countdown({
            date: '4/23/2021 03:00:00',
            offset: -6,
            day: 'Day',
            days: 'Days'
        });
    }

    // Sponsor Gallery Active Code
    if ($.fn.imagesLoaded) {
        $('.dawat-sponsor-content').imagesLoaded(function () {
            // filter items on button click
            $('.portfolio-menu').on('click', 'button', function () {
                var filterValue = $(this).attr('data-filter');
                $grid.isotope({
                    filter: filterValue
                });
            });

            // init Isotope
            var $grid = $('.dawat-sponsor-content').isotope({
                itemSelector: '.single-sponsor',
                percentPosition: true,
                masonry: {
                    columnWidth: '.single-sponsor'
                }
            });
        });
    }

    // Sponsor Menu Style Code
    $('.portfolio-menu button.btn').on('click', function () {
        $('.portfolio-menu button.btn').removeClass('active');
        $(this).addClass('active');
    })

    // Jarallax Active Code
    if ($.fn.jarallax) {
        $('.jarallax').jarallax({
            speed: 0.5
        });
    }

    // Speakers Active Code
    if ($.fn.flexslider) {
        $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            dot: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 68,
            maxItems: 6,
            asNavFor: '#slider',
            controlsContainer: $("#carousel"),
            customDirectionNav: $(".custom-navigation a"),
        });
        $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            directionNav: false,
            sync: "#carousel"
        });
    }

    // Testimonial Active Code
    if ($.fn.owlCarousel) {
        $('.dawat-testimonial-slide').owlCarousel({
            items: 2,
            margin: 80,
            dots: false,
            autoplay: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                }
            }
        });
    }

    // Gallery Active Code
    if ($.fn.flexslider) {
        $('.gallery_item').flexslider({
            animation: "slide",
            itemWidth: 174.98,
            itemMargin: 20,
            controlNav: true,
            animationLoop: false,
            slideshow: false,
            controlsContainer: $(".gallery_item"),
            customDirectionNav: $(".custom-navigation a"),
        });
    }

    // Scrollup Active Code
    if ($.fn.scrollUp) {
        dawatWindow.scrollUp({
            scrollSpeed: 1500,
            scrollText: '<i class="fa fa-angle-up"></i>'
        });
    }

    // Prevent Default 'a' Click
    $('a[href="#"]').on('click', function ($) {
        $.preventDefault();
    });

})(jQuery);